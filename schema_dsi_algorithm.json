{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://schema.arise-biodiversity.nl/dsi/v0/ai_algorithm.schema.json",
    "title": "AI algorithm",
    "description": "A trained deep learning algorithm including all pre- and post-processing to take media file(s) as input and produce species predictions.",
    "type": "object",
    "required": [
      "docker",
      "short_name",
      "version",
      "availability"
    ],
    "$comment": "'model' not required since some externally-provided docker images have no detailed model info.",
    "properties": {
      "api_type": {
        "type": "string",
        "enum": [
          "multiclass",
          "multilabel"
        ]
      },
      "docker": { "type": "string", "format": "iri" },
      "short_name": { "type": "string" },
      "description": { "type": "string" },
      "version": { "type": "string" },
      "availability": {
        "type": "string",
        "enum": [
          "public",
          "internal",
          "private"
        ]
      },
      "geographical_coverage": {
        "country_codes": {
          "type": "array",
          "items": { "type": "string" }
        },
        "multipolygon": "https://geojson.org/schema/MultiPolygon.json"
      },
      "taxonomic_coverage": {
        "type": "object",
        "description": "taxonomic_coverage is a dict, in which the KEY is a URI denoting the ontology/taxonomy in use, and the VALUE is an array of strings, each indicating a category within that ontology/taxonomny. It is acceptable to refer to broad classes (eg Aves).",
        "additionalProperties": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "uniqueItems": true
        }
      },
      "input": {
        "type": "object",
        "properties": {
          "media_modality": {
            "type": "array",
            "items": {
              "type": "string",
              "enum": [
                "image",
                "video",
                "audio",
                "radar"
              ]
            },
            "uniqueItems": true
          },
          "media_types": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "description": "Enumerated list of input media types (codecs) supported, e.g. ['image/png'].",
            "uniqueItems": true
          },
          "max_filesize_kb": {
            "type": "number",
            "description": "Maximum size of data passed in a single API query. Expressed in kilobytes.",
            "minimum": 0
          },
          "batch_support": {"type": "boolean"},
          "batch_size": {"type": "integer"}
        }
      },
      "configuration": {
        "type": "object",
        "properties": {
          "container": {
            "type": "object",
            "properties": {
              "infer_path": {"type": "string"},
              "infer_post_variable_name": {"type": "string"},
              "port": {"type": "integer"},
              "docker_runtime_parameters": {"type": "string"},
              "docker_env_parameters": {"type": "object"}
            }
          }
        }
      },
      "derived_from_algorithms": {
        "type": "array",
        "$comment": "This provenance info -- maybe in a specialist provenance layer, maybe follow PROV-O",
        "items": {
          "$ref": "https://schema.arise-biodiversity.nl/dsi/v0/ai_algorithm.schema.json"
        }
      },
      "authors": {
        "type": "array",
        "items": {
          "type": "string"
        }
      },
      "documentation": { "type": "string", "format": "iri" },
      "citation_info": { "type": "string" },
      "inference_costs": {
        "type": "array",
        "description": "Costs under typical conditions, on the hardware setup indicated. May eventually be made public e.g. for end-users to use in choosing algorithm. Expressed as the resource required to run ONE media item inference, even if batch use is typical.",
        "items": {
          "type": "object",
          "properties": {
            "hardware_shortname": { "type": "string" },
            "inference_time": {
              "exclusiveMinimum": 0,
              "description": "seconds",
              "type": "number"
            },
            "co2e_emissions": {
              "exclusiveMinimum": 0,
              "type": "number"
            }
          }
        }
      }
    }
}
