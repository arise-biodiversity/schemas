# Data formats for Arise DSI APIs

Please refer to the **JSON Schema file** for documentation in the format, for example `schema_dsi_algorithm_predictions.json` is the schema corresponding to the files `example_apioutput_*.json`. The schema is written using [JSON Schema](https://json-schema.org/) which makes it possible to automatically validate against the schema.

> *For example, go to https://www.jsonschemavalidator.net/ and paste the schema in the left side, and one of the examples in the right.*

To perform additional validation of your algorithm-predictions-JSON, use `python validate_dsi_predictions.py` -- this is a script which runs the basic JSON Schema validation as well as some extra checks such as getting the ID references correct.

<!-- ### TODO , and to connect to defined terms in other vocabularies (using [JSON-LD](https://json-ld.org/)). -->



## NOTES:

### Species/taxon labels: 

 * "taxon_id": a local or implementation-specific identifier for your algorithm e.g. "178@WRN".
 * "scientific_name": e.g. "Accipiter nisus". This field is identical to dwc:scientificName - definition: https://dwc.tdwg.org/list/#dwc_scientificName
 * "scientific_name_authorship": e.g. "(Györfi, 1952)". This field is identical to dwc:scientificNameAuthorship - definition: https://dwc.tdwg.org/list/#dwc_scientificNameAuthorship
 * "scientific_name_id": e.g. "https://observation.org/species/383/". A permanent public URI which acts as a resolvable identifier for the taxonomic category. This field is identical to dwc:scientificNameID - definition: https://dwc.tdwg.org/list/#dwc_scientificNameID


### How to refer to the same individual/object over multiple frames:

In simple cases, a single "media\_group" refers to one or more media files (e.g. images) or subregions (e.g. bounding boxes), and the media\_group's "id" is an identifier that allows you to give a species prediction that applies to the whole set. You may also be tracking individual animals across a sequence, so optionally you can *also* use "individual\_id" to give a label that enables your system to group multiple media\_group items as representing the same individual. Both these identifiers are opaque strings, so they can be project-specific, choose whatever labels you like.


### Multiclass versus multilabel:

Consider this example, with species and probabilities: `[["Blackbird", 0.7], ["Robin", 0.2]]`. Do we assume that:

1. these probabilities are mutually exclusive, and there is only one label to be given ("single-label multiclass classification"), or
2. these probabilities are independent, and both birds - or neither - might truly be present ("multi-label classification").

Some systems are trained for one, some for the other. We implement this within the `predictions` output by the tag `type: multiclass` or `type: multilabel`.



