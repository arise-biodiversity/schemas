#!/bin/env python

# Script to validate algorithm predictions output JSON against the Arise DSI schema

# by Dan Stowell, Nov 2022
# Public domain

import json, sys, pathlib
import jsonschema
from warnings import warn

######################################################
schemasrc = pathlib.Path(__file__).parent / "schema_dsi_predictions.json"
examplesrc = pathlib.Path(__file__).parent / "examples/example_apioutput_diopsis.json"


######################################################
with open(schemasrc, "rt") as infp:
    schema = json.load(infp)

######################################################


def validate_json_predictions_file(filepath):
    "Performs JSON schema validation, as well as additional checks of referential integrity. Prints warnings/errors, and returns a numerical value (0=OK, other val = rejected)"

    # Is the file interpretable as a JSON structure?
    with open(filepath, "rt") as infp:
        try:
            data = json.load(infp)
        except json.decoder.JSONDecodeError as err:
            print(
                "ERROR: Input file does not seem to be properly-formed JSON --- failed to decode (not tried validating yet)"
            )
            print(err)
            return 1

    # Does the JSON content validate against the schema?
    try:
        jsonschema.validate(instance=data, schema=schema)
    except jsonschema.exceptions.ValidationError as err:
        print(err)
        err = "ERROR: the supplied JSON data is invalid according to the schema"
        return 2

    # Referential integrity
    # - Does every predictions[]->region_group_id match a region_groups[]->id?
    # - Does every region_groups[]->regions[]->media_id match a media[]->id?
    # - Are there any un-referred-to? If so, raise a warning not an error.

    xrefs_p_r = set([pred["region_group_id"] for pred in data["predictions"]])
    xrefs_r_m = set(
        [r["media_id"] for rg in data["region_groups"] for r in rg["regions"]]
    )

    ids_r = set([r["id"] for r in data["region_groups"]])
    ids_m = set([m["id"] for m in data["media"]])

    mismatches = ids_r.difference(xrefs_p_r)
    if len(mismatches) > 0:
        print(f"WARNING: Found {len(mismatches)} region_groups[]->id not used")
    mismatches = xrefs_p_r.difference(ids_r)
    if len(mismatches) > 0:
        print(
            f"ERROR: Found {len(mismatches)} predictions[]->region_group_id without a corresponding region_groups[]->id"
        )
        for mm in mismatches:
            print(mm)
        return 3
    mismatches = ids_m.difference(xrefs_r_m)
    if len(mismatches) > 0:
        print(f"WARNING: Found {len(mismatches)} media[]->id not used")
    mismatches = xrefs_r_m.difference(ids_m)
    if len(mismatches) > 0:
        print(
            f"ERROR: Found {len(mismatches)} region_groups[]->regions[]->media_id without a corresponding media[]->id"
        )
        for mm in mismatches:
            print(mm)
        return 3

    # Dimensions expressions
    # - Does every region_groups[]->regions[]->box contain fieldgroups x1/x2/y1/y2 OR t1/t2? If so, warn (not error).
    #         (If there is no box, no problem; if there is a box but no fieldgroups, this is an error.)
    # - Does every x1/y1/t1 have a matching x2/y2/t2? ERROR if not.
    # - Does every pair x1/x2 y1/y2 t1/t2 have an "<=" relationship? ERROR if not.
    # - Does every x1 have a matching y1? ERROR if not.
    # Note that it's fine to have no box at all. This is equivalent to a prediction for the entire image/sound.
    for rg in data["region_groups"]:
        for r in rg["regions"]:
            if "box" in r:
                rb = r["box"]
                founddims = False
                for whichdim in ["x", "y", "t"]:
                    if f"{whichdim}1" in rb:
                        if f"{whichdim}2" not in rb:
                            print(
                                f"ERROR: region box has {whichdim}1 but not {whichdim}2"
                            )
                            print(r)
                            return 4
                        elif rb[f"{whichdim}1"] > rb[f"{whichdim}2"]:
                            print(
                                f"ERROR: region box has {whichdim}1 greater than {whichdim}2"
                            )
                            print(r)
                            return 4
                        founddims = True
                if not founddims:
                    print(
                        "ERROR: region has a 'box' but it contains neither spatial (x1,y1) nor temporal (t1) extents"
                    )
                    print(r)
                    return 4
                if ("x1" in rb) and ("y1" not in rb):
                    print("ERROR: region box has x1 but not y1")
                    print(r)
                    return 4

    print(f"JSON data is OK! {filepath}")
    return 0


######################################################
if __name__ == "__main__":
    src = examplesrc
    if len(sys.argv) > 1:
        src = sys.argv[1]
    retval = validate_json_predictions_file(src)
    sys.exit(retval)
